import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:lv8/models/item.dart';
import 'package:lv8/widgets/item_card.dart';
import 'package:http/http.dart' as http;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController _textEditingController = TextEditingController();
  Widget body = Container();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff028677),
        title: Text("LV3"),
        centerTitle: false,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: _textEditingController,
                    decoration: InputDecoration(
                      hintText: "Company name",
                    ),
                  ),
                ),
                SizedBox(width: 16),
                FlatButton(
                  color: Color(0xffd5d7d6),
                  onPressed: () {
                    setState(() {
                      body = FutureBuilder(
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting)
                            return Center(child: CircularProgressIndicator());
                          else if (snapshot.data == null)
                            return Container();
                          else if (snapshot.data.length == 0)
                            return Container(
                                child: Center(child: Text("Nema rezultata")));
                          return _buildList(snapshot.data);
                        },
                        future: _getItems(_textEditingController.text),
                      );
                      _textEditingController.clear();
                    });
                  },
                  child: Text("SEARCH"),
                ),
              ],
            ),
            Expanded(
              child: body,
            ),
          ],
        ),
      ),
    );
  }

  Future<List<Item>> _getItems(String searchTerm) async {
    final url =
        "http://makeup-api.herokuapp.com/api/v1/products.json?brand=$searchTerm";

    final response = await http.get(url);

    List<Item> result = [];
    if (response.statusCode == 200) {
      final parsedJson = json.decode(response.body);
      for (int i = 0; i < (parsedJson as List).length; i++) {
        result.add(Item.fromJson(parsedJson[i]));
      }
      return result;
    }

    return null;
  }

  Widget _buildList(List<Item> items) => ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return ItemCard(items[index]);
        },
      );
}
